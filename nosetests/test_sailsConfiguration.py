from sailsConfiguration import *
import os
import json

os.chdir("./src/")

def test_get_server_creation_command():
    assert get_server_creation_command("zombie") == "sails new zombie > /dev/null"

def test_get_generate_api_command():
    assert get_generate_api_command("zombie", "controllername") == "cd zombie && sails generate api controllername > /dev/null"

def test_get_path_to_api_controller():
    assert get_path_to_api_controller("zombie","controllername") ==  "./zombie/api/controllers/controllernameController.js"


def test_api_format():
    x, y = api_format("nmap {OPTIONS} {IP}")
    assert y == ["OPTIONS", "IP"]
    assert x == "nmap {0} {1}"

    x, y = api_format("nmap {OPTIONS} {IP} {ANOTHER} {lowercase}")
    assert y == ["OPTIONS", "IP", "ANOTHER", "lowercase"]
    assert x == "nmap {0} {1} {2} {3}"

def test_api_format_empty():
    x, y = api_format("ls")
    assert y == []
    assert x == "ls"

def test_endpoint_string():
    f = open("../nosetests/controller_endpoint_string.js")
    tmp = ""
    for line in f:
        tmp = tmp + line
    print(tmp)
    print(endpoint_string("scan", "nmap {OPTIONS} {IP}"))
    assert tmp == endpoint_string("scan", "nmap {OPTIONS} {IP}")

def test_get_REST_request():
    parsed_json = json.load(open("../mutation_file_examples/mutation_file_example.json"))
    print(get_REST_request(parsed_json))
    assert ['<li><span>/Controller/scan?OPTIONS=x&IP=x</span></li>'] == get_REST_request(parsed_json)
