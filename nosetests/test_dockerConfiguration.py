from dockerConfiguration import *

def test_get_build_container_command():
    assert get_build_container_command("zombie") == "docker build zombie -t \"zombie\""
