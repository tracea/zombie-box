from numpy import *
from blessings import Terminal

def helix():
    """This function creates a double helix used 
    """
    amp = 10
    length = 100
    wavelength = 20

    omega = (2*pi)/wavelength
    phi   = wavelength*(0.5)
    X = arange(1,length)
    Y1 = round_(amp*(sin(omega*X) + 1))
    Y2 = round_(amp*(sin(omega*X+phi) + 1))

    offset = phi/2
    Z1 = sin(omega*X + offset)
    Z2 = sin(omega*X + phi + offset)

    T1 = " ######### "
    T2 = " ********* "
    clen = len(T1)

    H = zeros((length,amp*2+clen),dtype='str')
    H[:,:] = " "

    for n,(y1,y2,z1,z2) in enumerate(zip(Y1,Y2,Z1,Z2)):
        H[n,y1:y1+clen] = list(T1)
        H[n,y2:y2+clen] = list(T2)

        # Overwrite if first helix is on top
        if z1>z2: H[n,y1:y1+clen] = list(T1)

    return H


def progress():
    term = Terminal()
    term.clear()

    H = helix()
    H = H[:-1]
    M = [" ."," ."," ."," . ."," . ."," . .", " . . ."," . . ."," . . ."]
    
    while(GLOBAL_RUN):
        term.location()
        print_helix(H,term,M)
        time.sleep(0.1)
        tmp = H[1:]
        H = vstack((tmp,H[0]))
        
        tmp = M[1:]
        tmp.append(M[0])
        M = tmp

        
def print_helix(H,term,M):
    for i in range(term.height-1):
        line = ""
        if (i == 2):
            line = "".join(H[i]) + " MUTATING" + M[0]
        elif (i == 4):
            line = "".join(H[i]) + " " + GLOBAL_STATUS
        else:
            line = "".join(H[i])
        with term.location(0,i):
            print(line + " " * (term.width-len(line)))
