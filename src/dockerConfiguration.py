import sys
import os

def write_from_file(path,f):
    """ Takes a file and a path to a new file and copies everything
    from the file at path, to the open file f.
    """
    to_read = open(path)
    for line in to_read:
        f.write(line)

    to_read.close()

def get_build_container_command(name):
    return "docker build {} -t \"{}\"".format(name,name)

def generate_dockerfile(mutations):
    f = open(mutations['server_name'] + "/Dockerfile",'w')
    write_from_file("file_templates/dockerfile_header",f)

    dependencies = set()

    for e in mutations["endpoints"]:
        for dep in e["dependencies"]:
            dependencies.add(dep)

    for e in dependencies:
        f.write("RUN " + e + "\n")

    write_from_file("file_templates/dockerfile_footer",f)
    f.close()

def docker_config(parsed_mutations):
    generate_dockerfile(parsed_mutations)
    os.system(get_build_container_command(parsed_mutations['server_name']))
