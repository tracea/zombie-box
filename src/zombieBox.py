import sys
import json
import sailsConfiguration
import dockerConfiguration
from threading import Thread


def mutate(mutation_file,load_screen):
    """ Runs the entire ZombieBox creation process.
    Arguments:
    - mutation_file: path to mutation file of the desired zombie box.
    - load_screen: bool telling you if you should run the fancy loading screen.
    """

    if(load_screen):
        pass

    parsed_mutations = json.load(open(mutation_file))
    sailsConfiguration.sails_config(parsed_mutations) # configure and create web server
    dockerConfiguration.docker_config(parsed_mutations) # configure and build docker container

if __name__ == "__main__":
    load_screen = True
    try:
        from loadingScreen import progress
    except ImportError:
        load_screen = False

    mutate(sys.argv[1],load_screen)
