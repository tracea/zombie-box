import os

def get_server_creation_command(name):
    """ returns command to create a new sails web server with argument
    supplied.
    """
    
    return "sails new {} > /dev/null".format(name)

def get_generate_api_command(server_name, api_name):
    # Creates command to generate API
    return "cd {} && sails generate api {} > /dev/null".format(server_name,api_name)

def get_path_to_api_controller(server_name, api_name):
    # returns the path to the API Controller file
    return "./{}/api/controllers/{}Controller.js".format(server_name, api_name)


def api_format(api):
    """ This functions returns the parameters of an API defined
    int the mutation file and the string needed for the corresponding
    javascript to make API calls. 

    Arguments:
    - api: An API endpoint defined in a mutation file

    Returns:
    - params: API parameters
    - exec_str: javascript string to later be formatted.
    """
    params = []
    exec_str = ""
    record = True
    counter = 0
    param = ""
    
    for e in api:
        if (e == "}"):
            params.append(param)
            param = ""
            record = True
            
        if (record):
            exec_str = exec_str + e

        else:
            param = param + e
                    
        if (e == "{"):
            record = False
            exec_str = exec_str + str(counter)
            counter = counter + 1

    return exec_str, params


def endpoint_string(name, api):
    """ returns the javascript of the api to be created
    """
    return_str = "{}: function(req, res) {{\n".format(name)
    
    exec_str, params = api_format(api)

    for e in params:
        return_str = return_str + "var {} = req.param('{}');\n".format(e,e)

    return_str = return_str + "var to_exec = '{}'.format(".format(exec_str)

    for e in params[:-1]:
        return_str = return_str + e + ","

    return_str = return_str + params[-1] + ");\n"
    
    f = open("file_templates/api_execution_footer.js")
    for line in f:
        return_str = return_str + line
    f.close()
    
    return return_str

def write_from_file(path,f):
    """ Takes a file and a path to a new file and copies everything
    from the file at path, to the open file f.
    """ 
    to_read = open(path)
    for line in to_read:
        f.write(line)

    to_read.close()
        
def get_html_endpoint(endpoint):
    # formats endpoint to be put in a list in the default webpage
    return "<li><span>{}</span></li>".format(endpoint);
    
def get_REST_request(parsed_json):
    """ gets HTTP requests that could be made to the API's defined in the
    mutation file.
    """
    result = []
    for e in parsed_json["endpoints"]:
        stem = "/{}/{}?".format(parsed_json["api_name"], e["name"])
        unused, params = api_format(e["api"]) #not using first return value in this case
        param_str = ""
        
        for e in params:
            if (param_str == ""): #Catches first parameter
                param_str = e + "=x"
            else:
                param_str = param_str + "&" + e + "=x"
        result.append(get_html_endpoint(stem + param_str))
        
    return result

def configure_home_page(parsed_json):
    """ Responsible for generating a default ZombieBox home page.
    The default page includes information on what APIs are available.

    Arguments:
    - parsed_json: The parsed mutation file supplied on the command line.
    """
    
    to_read = open("{}/assets/index.html".format(parsed_json["server_name"]))
    to_write = open("{}/assets/index.html_tmp".format(parsed_json["server_name"]),"w")
    web_page = [[],[],[]]
    is_header = True

    
    for line in to_read:
        if ("placeholder" in line):
            """deliberately not adding this line to the list
            to remove it from webpage """
            
            is_header = False
        else:
            if (is_header):
                web_page[0].append(line)
            else:
                web_page[2].append(line)

    web_page[1] = get_REST_request(parsed_json)
        
    to_read.close()
    
    for to_write_list in web_page:
        for e in to_write_list:
            to_write.write(e)
            
    to_write.close()
    os.system("mv {}/assets/index.html_tmp {}/assets/index.html".format(parsed_json["server_name"],parsed_json["server_name"]))
    
def configure_api(mutations, api_file):
    """ Writes the API endpoints in javascript to the API controller file
    """
    
    f = open(api_file,"w")

    write_from_file("file_templates/api_header.js",f)
    
    for e in mutations[:-1]:
        f.write(endpoint_string(e["name"],e["api"]) + ",\n")

    f.write(endpoint_string(mutations[-1]["name"],mutations[-1]["api"]) + "\n")
        
    write_from_file("file_templates/api_footer.js",f)

    f.close()


def deal_with_configuration(name):
    """ Function that further configures the SailsJS Webserver.
    This function installs shelljs and generates a static homepage.
    Note: The static homepage is overwritten completely by the
    configure_home_page function.

    Arguments:
    - name: path to the web server
    """
    
    f = open(name+"/config/models.js","w")
    f.write("module.exports.models = { migrate: 'alter' };")
    f.close()

    os.system("cd {} && npm install -save shelljs > /dev/null".format(name))
    os.system("cd {} && npm install -save sails-generate-static > /dev/null".format(name))
    os.system("cd {} && sails generate static > /dev/null".format(name))
    os.system("cp -r file_templates/webpage/* {}/assets/ > /dev/null".format(name))


def sails_config(parsed_mutations):
    """ Creates and configures a SailsJS webserver as defined in the supplied
    mutation file.
    """
    os.system(get_server_creation_command(parsed_mutations['server_name']))
    
    os.system(get_generate_api_command(parsed_mutations['server_name'],
                                       parsed_mutations['api_name']))
    
    configure_api(parsed_mutations["endpoints"],
                  get_path_to_api_controller(parsed_mutations['server_name'],
                                             parsed_mutations['api_name']))
    
    deal_with_configuration(parsed_mutations['server_name'])
    
    configure_home_page(parsed_mutations)
